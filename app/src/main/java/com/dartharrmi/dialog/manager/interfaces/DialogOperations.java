package com.dartharrmi.dialog.manager.interfaces;

import android.view.View;

public interface DialogOperations {

    void onNeutralAction();

    void onNeutralAction(View view);

    void onPositiveAction();

    void onPositiveAction(View view);

    void onNegativeAction();

    void onNegativeAction(View view);

}

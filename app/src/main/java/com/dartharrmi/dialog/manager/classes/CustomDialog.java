package com.dartharrmi.dialog.manager.classes;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.dartharrmi.dialog.manager.R;
import com.dartharrmi.dialog.manager.utils.DialogUtils;

public class CustomDialog extends Dialog {

    private boolean neutralButton;
    private boolean positiveButton;
    private boolean negativeButton;
    private String neutralText;
    private String positiveText;
    private String negativeText;
    private View view;

    public static CustomDialog newInstance(@NonNull Bundle bundle, View view) {
        CustomDialog customDialog = new CustomDialog();
        customDialog.setArguments(bundle);
        customDialog.setView(view);

        return customDialog;
    }

    public CustomDialog() {
        super();
    }

    private void setView(View view) {
        this.view = view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            neutralButton = savedInstanceState.getBoolean(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL);
            positiveButton = savedInstanceState.getBoolean(DialogUtils.ARG_DIALOG_BUTTON_POSITIVE);
            negativeButton = savedInstanceState.getBoolean(DialogUtils.ARG_DIALOG_BUTTON_NEGATIVE);

            neutralText = savedInstanceState.getString(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL_TEXT);
            positiveText = savedInstanceState.getString(DialogUtils.ARG_DIALOG_BUTTON_POSITIVE_TEXT);
            negativeText = savedInstanceState.getString(DialogUtils.ARG_DIALOG_BUTTON_NEGATIVE_TEXT);

            title = savedInstanceState.getString(DialogUtils.ARG_DIALOG_TITLE);
            text = savedInstanceState.getString(DialogUtils.ARG_DIALOG_TEXT);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            cancelable = arguments.getBoolean(DialogUtils.ARG_DIALOG_CANCELABLE);
            neutralButton = arguments.getBoolean(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL);
            positiveButton = arguments.getBoolean(DialogUtils.ARG_DIALOG_BUTTON_POSITIVE);
            negativeButton = arguments.getBoolean(DialogUtils.ARG_DIALOG_BUTTON_NEGATIVE);

            neutralText = arguments.getString(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL_TEXT);
            positiveText = arguments.getString(DialogUtils.ARG_DIALOG_BUTTON_POSITIVE_TEXT);
            negativeText = arguments.getString(DialogUtils.ARG_DIALOG_BUTTON_NEGATIVE_TEXT);

            title = arguments.getString(DialogUtils.ARG_DIALOG_TITLE);
            text = arguments.getString(DialogUtils.ARG_DIALOG_TEXT);
        }
    }

    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        Builder b = new Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        b.setCancelable(cancelable);
        b.setTitle(title);
        b.setMessage(text);
        b.setView(view);

        if (neutralButton) {
            if (neutralText == null) {
                neutralText = "";
            }
            b.setNeutralButton(neutralText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    neutralAction(view);
                }
            });
        }

        if (positiveButton) {
            if (positiveText == null) {
                positiveText = "";
            }
            b.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    positiveAction(view);
                }
            });
        }
        if (negativeButton) {
            if (negativeText == null) {
                negativeText = "";
            }
            b.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    negativeAction(view);
                }
            });
        }
        return b.create();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL, neutralButton);
        outState.putBoolean(DialogUtils.ARG_DIALOG_BUTTON_POSITIVE, positiveButton);
        outState.putBoolean(DialogUtils.ARG_DIALOG_BUTTON_NEGATIVE, negativeButton);

        outState.putString(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL_TEXT, neutralText);
        outState.putString(DialogUtils.ARG_DIALOG_BUTTON_POSITIVE_TEXT, positiveText);
        outState.putString(DialogUtils.ARG_DIALOG_BUTTON_NEGATIVE_TEXT, negativeText);
    }
}

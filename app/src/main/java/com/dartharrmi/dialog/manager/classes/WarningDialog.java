package com.dartharrmi.dialog.manager.classes;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;

import com.dartharrmi.dialog.manager.R;
import com.dartharrmi.dialog.manager.utils.DialogUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * A dialog that shows an icon alongside the text.
 */
public class WarningDialog extends Dialog {

    /**
     * Annotations to sets the dialog type and show the correct icon in the Dialog.
     */
    @IntDef({DialogType.DEFAULT, DialogType.INFORMATION, DialogType.QUESTION, DialogType.WARNING,
            DialogType.ERROR, DialogType.DATABASE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface DialogType {
        int DEFAULT = 0;
        int INFORMATION = 1;
        int QUESTION = 2;
        int WARNING = 3;
        int ERROR = 4;
        int DATABASE = 5;
    }

    @DialogType
    private int dialogType;

    /**
     * Creates a new instance of {@link WarningDialog} class.
     *
     * @param bundle     Arguments of the Dialog, must include title and text.
     * @param dialogType The dialog type used to show an icon.
     * @return a new instance of {@link WarningDialog}
     * @see {@link DialogType}
     */
    public static WarningDialog newInstance(@NonNull Bundle bundle, @DialogType int dialogType) {
        WarningDialog warningDialog = new WarningDialog();
        warningDialog.setArguments(bundle);
        warningDialog.setDialogType(dialogType);

        return warningDialog;
    }

    /**
     * Required default constructor.
     */
    public WarningDialog() {
    }

    /**
     * Sets the dialog type.
     *
     * @param dialogType The Dialog type
     * @see {@link DialogType}
     */
    public void setDialogType(@DialogType int dialogType) {
        this.dialogType = dialogType;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            title = savedInstanceState.getString(DialogUtils.ARG_DIALOG_TITLE);
            text = savedInstanceState.getString(DialogUtils.ARG_DIALOG_TEXT);
            dialogType = savedInstanceState.getInt("DIALOG_TYPE");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments != null) {
            title = arguments.getString(DialogUtils.ARG_DIALOG_TITLE);
            text = arguments.getString(DialogUtils.ARG_DIALOG_TEXT);
        }
    }

    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        Builder b = new Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        b.setCancelable(cancelable);
        b.setTitle(title);
        b.setMessage(text);
        switch (dialogType) {
            case DialogType.INFORMATION:
                b.setIcon(R.drawable.information);
                break;
            case DialogType.QUESTION:
                b.setIcon(R.drawable.question);
                b.setCancelable(false);
                break;
            case DialogType.ERROR:
                b.setIcon(R.drawable.error);
                break;
            case DialogType.DATABASE:
                b.setIcon(R.drawable.database);
                break;
            case DialogType.WARNING:
                b.setIcon(R.drawable.exclamation);
                break;
            case DialogType.DEFAULT:
            default:
                break;
        }
        b.setNeutralButton(R.string.st_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                neutralAction();
            }
        });
        return b.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("DIALOG_TYPE", dialogType);
    }
}

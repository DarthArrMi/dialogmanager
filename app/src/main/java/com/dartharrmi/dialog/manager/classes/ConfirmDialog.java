package com.dartharrmi.dialog.manager.classes;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.dartharrmi.dialog.manager.R;
import com.dartharrmi.dialog.manager.interfaces.DialogOperations;
import com.dartharrmi.dialog.manager.utils.DialogUtils;

/**
 * A dialog used to confirm actions.
 */
public class ConfirmDialog extends Dialog {

    private boolean cancelButton;

    private String neutralText;
    private String positiveText;
    private String negativeText;

    /**
     * Returns a new instance of a {@link ConfirmDialog}
     *
     * @param bundle The arguments of the Fragment.
     * @return a new instance of {@link ConfirmDialog}
     */
    public static ConfirmDialog newInstance(@NonNull Bundle bundle) {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.setArguments(bundle);

        return confirmDialog;
    }

    /**
     * Default constructor.
     */
    public ConfirmDialog() {
        super();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            cancelButton = savedInstanceState.getBoolean(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL);
            title = savedInstanceState.getString(DialogUtils.ARG_DIALOG_TITLE);
            text = savedInstanceState.getString(DialogUtils.ARG_DIALOG_TEXT);

            if (cancelButton) {
                neutralText = savedInstanceState.getString(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL_TEXT);
            }
            positiveText = savedInstanceState.getString(DialogUtils.ARG_DIALOG_BUTTON_POSITIVE_TEXT);
            negativeText = savedInstanceState.getString(DialogUtils.ARG_DIALOG_BUTTON_NEGATIVE_TEXT);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof DialogOperations && adapter.getOperations() == null) {
            setOperations((DialogOperations) activity);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments != null) {
            cancelButton = arguments.getBoolean(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL);
            title = arguments.getString(DialogUtils.ARG_DIALOG_TITLE);
            text = arguments.getString(DialogUtils.ARG_DIALOG_TEXT);

            if (cancelButton) {
                neutralText = savedInstanceState.getString(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL_TEXT,
                        getResources().getString(R.string.st_cancel));
            }
            positiveText = arguments.getString(DialogUtils.ARG_DIALOG_BUTTON_POSITIVE_TEXT,
                    getResources().getString(R.string.st_yes));
            negativeText = arguments.getString(DialogUtils.ARG_DIALOG_BUTTON_NEGATIVE_TEXT,
                    getResources().getString(R.string.st_no));
        }
    }

    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        Builder builderConfirmation = new Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builderConfirmation.setCancelable(cancelable);
        builderConfirmation.setTitle(title);
        builderConfirmation.setMessage(text);
        builderConfirmation.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        positiveAction();
                    }
                });
        builderConfirmation.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        negativeAction();
                    }
                });
        if (cancelButton) {
            builderConfirmation.setNeutralButton(neutralText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            neutralAction();
                        }
                    });
        }

        return builderConfirmation.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL, cancelButton);
        if (cancelButton) {
            outState.putString(DialogUtils.ARG_DIALOG_BUTTON_NEUTRAL_TEXT, neutralText);
        }
        outState.putString(DialogUtils.ARG_DIALOG_BUTTON_POSITIVE_TEXT, positiveText);
        outState.putString(DialogUtils.ARG_DIALOG_BUTTON_NEGATIVE_TEXT, negativeText);
    }
}
package com.dartharrmi.dialog.manager.abstracts;

import android.view.View;

import com.dartharrmi.dialog.manager.interfaces.DialogOperations;

public class DialogOperationAdapter implements DialogOperations {

	private DialogOperations operations;

	public DialogOperations getOperations() {
		return operations;
	}

	public void setOperations(DialogOperations operations) {
		this.operations = operations;
	}

	@Override
	public void onNeutralAction() {
		if (operations != null) {
			operations.onNeutralAction();
		}
	}

	@Override
	public void onNeutralAction(View view) {
		if (operations != null) {
			operations.onNeutralAction(view);
		}
	}

	@Override
	public void onPositiveAction() {
		if (operations != null) {
			operations.onPositiveAction();
		}
	}

	@Override
	public void onPositiveAction(View view) {
		if (operations != null) {
			operations.onPositiveAction(view);
		}
	}

	@Override
	public void onNegativeAction() {
		if (operations != null) {
			operations.onNegativeAction();
		}
	}

	@Override
	public void onNegativeAction(View view) {
		if (operations != null) {
			operations.onNegativeAction(view);
		}
	}
}

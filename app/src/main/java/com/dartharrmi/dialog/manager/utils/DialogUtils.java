package com.dartharrmi.dialog.manager.utils;

public class DialogUtils {

    public static final String CONFIRM_DIALOG = "confirm_dialog";
    public static final String CUSTOM_DIALOG = "custom_dialog";
    public static final String PROGRESS_DIALOG = "progress_dialog";
    public static final String WARNING_DIALOG = "warning_dialog";

    public static final String ARG_DIALOG_CANCELABLE = "ARG_DIALOG_CANCELABLE";
    public static final String ARG_DIALOG_BUTTON_NEGATIVE = "ARG_DIALOG_BUTTON_NEGATIVE";
    public static final String ARG_DIALOG_BUTTON_NEUTRAL = "ARG_DIALOG_BUTTON_NEUTRAL";
    public static final String ARG_DIALOG_BUTTON_POSITIVE = "ARG_DIALOG_BUTTON_POSITIVE";
    public static final String ARG_DIALOG_BUTTON_NEGATIVE_TEXT = "ARG_DIALOG_BUTTON_NEGATIVE_TEXT";
    public static final String ARG_DIALOG_BUTTON_NEUTRAL_TEXT = "ARG_DIALOG_BUTTON_NEUTRAL_TEXT";
    public static final String ARG_DIALOG_BUTTON_POSITIVE_TEXT = "ARG_DIALOG_BUTTON_POSITIVE_TEXT";

    public static final String ARG_DIALOG_TITLE = "ARG_DIALOG_TITLE";
    public static final String ARG_DIALOG_TEXT = "ARG_DIALOG_TEXT";
}

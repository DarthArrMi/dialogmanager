package com.dartharrmi.dialog.manager.classes;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.View;

import com.dartharrmi.dialog.manager.abstracts.DialogOperationAdapter;
import com.dartharrmi.dialog.manager.interfaces.DialogOperations;
import com.dartharrmi.dialog.manager.utils.DialogUtils;

public class Dialog extends DialogFragment {

    protected boolean cancelable;
    protected String title;
    protected DialogOperationAdapter adapter;

    protected String text;

    private FragmentManager manager;

    public Dialog() {
        adapter = new DialogOperationAdapter();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setOperations(DialogOperations operations) {
        adapter.setOperations(operations);
    }

    @Override
    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        this.manager = manager;
        if (this.manager != null) {
            this.manager.beginTransaction().add(this, tag).commitAllowingStateLoss();
        }
    }

    @Override
    public void dismiss() {
        if (manager != null) {
            manager.beginTransaction().remove(this).commitAllowingStateLoss();
        } else {
            super.dismiss();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(DialogUtils.ARG_DIALOG_CANCELABLE, cancelable);
        outState.putString(DialogUtils.ARG_DIALOG_TITLE, title);
        outState.putString(DialogUtils.ARG_DIALOG_TEXT, text);
    }

    public void neutralAction() {
        if (adapter.getOperations() != null) {
            adapter.getOperations().onNeutralAction();
        }
        dismiss();
    }

    public void neutralAction(View view) {
        if (adapter.getOperations() != null) {
            adapter.getOperations().onNeutralAction(view);
        }
        dismiss();
    }

    public void positiveAction() {
        if (adapter.getOperations() != null) {
            adapter.getOperations().onPositiveAction();
        }
        dismiss();
    }

    public void positiveAction(View view) {
        if (adapter.getOperations() != null) {
            adapter.getOperations().onPositiveAction(view);
        }
        dismiss();
    }

    public void negativeAction() {
        if (adapter.getOperations() != null) {
            adapter.getOperations().onNegativeAction();
        }
        dismiss();
    }

    public void negativeAction(View view) {
        if (adapter.getOperations() != null) {
            adapter.getOperations().onNegativeAction(view);
        }
        dismiss();
    }
}

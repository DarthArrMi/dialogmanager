package com.dartharrmi.dialog.manager.classes;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.view.KeyEvent;

import com.dartharrmi.dialog.manager.R;

public class LoadingDialog extends Dialog {

	private boolean cancelableTouch;
	private boolean indeterminate;

	private Thread task;

	public LoadingDialog(String text, Thread task) {
		super();
		setText(text);
		this.task = task;
	}

	public void setTask(Thread task) {
		this.task = task;
	}

	public void setCancelableOnTouchOutside(boolean cancelableTouch) {
		this.cancelableTouch = cancelableTouch;
	}

	public void setIndeterminate(boolean indeterminate) {
		this.indeterminate = indeterminate;
	}

	@Override
	public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);

		ProgressDialog dialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
		dialog.setMessage(text);
		dialog.setIndeterminate(indeterminate);
		dialog.setCancelable(cancelable);
		dialog.setCanceledOnTouchOutside(cancelableTouch);
		dialog.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				return keyCode == KeyEvent.KEYCODE_BACK;
			}
		});
		return dialog;
	}

	@Override
	public void show(FragmentManager manager, String tag) {
		super.show(manager, tag);
		if (task != null) {
			task.start();
		}
	}
}
